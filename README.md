### Arguments:
#### employeeId*: 
login to codebase, open the browser console, select the network tab, 
select a 'nincs allokacio' on the page 
and search the employeeId in payload of the AttendenceTracking with event of GetEfficientWorking where employeeId provided. 

Usage: 

employeeId=1234567

#### cookie*: 
Copy the entire cookie from the Codebase console.

Usage:

cookie=ASP.NET_SessionId=5ibkc5k4al3mt3y4tcih53hb;
.ASPXAUTHUD=Pg4Asbolj83FmbNsRL8MZEqRroejEMgkhj2bumiw3JZmE/9SYYNHLt2RbLzpf6XYfcTnSw2yHK7I2j4oU7MWaC1uYNaem0MSsjtGVlIL6Tws/U6mlDMXA79od3iiHEn1;
.ASPXAUTH=5C9D5ADE850471F4123DCF888522214A4A61E83939154962150FC205A005CBA2F35CBA51FF08C5340EC4189AA30F7A1B761239E92A5D34EAFF6010F3ABFAEE4C34BD68181EA85A7289D052CCBA5C8811006D2D99895E962908EC8F2363011BDAC90FDEBC6931C1BF8A5CA617263E65C69E54F6001DCECB792E82AEC4C9366239

#### month: 
You can specify a month, more months or range.

Usage:

month=1 (January)

month=1-6 (from January to June)

month=1,3,5 (January, March and May)

Default: Current month

The day automatically calculate based on the month

#### year: 
You can specify a year.
Usage:
year=2022
Default: Current year

\* is required

#### Example:
``
  employeeId=209803
  cookie=ASP.NET_SessionId=5ibkc5k4al3mt3y4tcih53hb;
  .ASPXAUTHUD=Pg4Asbolj83FmbNsRL8MZEqRroejEMgkhj2bumiw3JZmE/9SYYNHLt2RbLzpf6XYfcTnSw2yHK7I2j4oU7MWaC1uYNaem0MSsjtGVlIL6Tws/U6mlDMXA79od3iiHEn1;
  .ASPXAUTH=5C9D5ADE850471F4123DCF888522214A4A61E83939154962150FC205A005CBA2F35CBA51FF08C5340EC4189AA30F7A1B761239E92A5D34EAFF6010F3ABFAEE4C34BD68181EA85A7289D052CCBA5C8811006D2D99895E962908EC8F2363011BDAC90FDEBC6931C1BF8A5CA617263E65C69E54F6001DCECB792E82AEC4C9366239
  month=8-12
  year=2022
``

### Run
Import into the idea and run the `CodebaseTrackerApp.main`