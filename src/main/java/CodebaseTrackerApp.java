import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class CodebaseTrackerApp {

    private static final int CONNECT_TIMEOUT_MS = 20_000;

    ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) {
        try {
            String employeeIdString = null;
            StringBuilder cookie = new StringBuilder();
            String monthString = null;
            String yearString = null;
            for (String arg : args) {
                if (arg.startsWith("employeeId=")) {
                    employeeIdString = arg.replace("employeeId=", "");
                }
                if (arg.startsWith("cookie=")) {
                    cookie.append(arg.replace("cookie=", ""));
                }
                if (arg.startsWith("month=")) {
                    monthString = arg.replace("month=", "");
                }
                if (arg.startsWith("year=")) {
                    yearString = arg.replace("year=", "");
                }
            }
            if (employeeIdString == null) {
                throw new RuntimeException("Please provide an employee id as args (login to codebase, open the browser console, select a 'nincs allokacio', " +
                        "search the employeeId in payload where employeeId provided: employeeId=1234567");
            }
            if (cookie.length() == 0) {
                throw new RuntimeException("Please provide a cookie as args (login to codebase and copy-paste here: cookie=any-cookie-string");
            }
            List<Integer> monthes;
            int year;
            if (monthString == null || monthString.equals("")) {
                System.out.println("Month is not provided, use the current month");
                monthes = List.of(calculateCurrentMonth());
            } else {
                monthes = getMonths(monthString);
            }
            if (yearString == null || yearString.equals("")) {
                System.out.println("Year is not provided, use the current year");
                year = calculateCurrentYear();
            } else {
                year = Integer.parseInt(yearString);
            }
            int employeeId = Integer.parseInt(employeeIdString);
            CodebaseTrackerApp codebaseTrackerApp = new CodebaseTrackerApp();
            for (Integer month : monthes) {
                int endDay = calculateEndDayOfMonth(year, month);
                System.out.printf("Add cost to period: %s-%02d-01 to %s-%02d-%02d%n%s", year, month, year, month, endDay, System.lineSeparator());
                codebaseTrackerApp.run(year, month, 1, endDay, employeeId, cookie.toString());
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    static List<Integer> getMonths(String monthString) {
        List<Integer> months = new ArrayList<>();
        if (monthString.contains("-")) {
            String[] split = monthString.split("-");
            int start = Integer.parseInt(split[0]);
            int end = Integer.parseInt(split[1]);
            for (int i = start; i <= end; i++) {
                months.add(i);
            }
        } else {
            months.addAll(Arrays.stream(monthString.split(","))
                    .map(String::trim)
                    .map(Integer::parseInt)
                    .collect(Collectors.toSet()));
        }
        return months;
    }

    private static int calculateCurrentMonth() {
        LocalDate date = LocalDate.now();
        return date.getMonthValue();
    }

    private static int calculateCurrentYear() {
        LocalDate date = LocalDate.now();
        return date.getYear();
    }

    private static int calculateEndDayOfMonth(int year, int month) {
        LocalDate date = LocalDate.of(year, month, 1);
        return date.getMonth().length(date.isLeapYear());
    }

    public void run(int year, int month, int startDay, int endDay, int employeeId, String cookie) throws JsonProcessingException {

        List<GetDayViewEvent> predefinedDates = getPredefinedDates(year, month, startDay, endDay, cookie);
        System.out.println(predefinedDates.size() + " days with `Nincs allokacio`");
        if (!predefinedDates.isEmpty()) {
            System.out.println(predefinedDates.size() + " predefined dates.");
            addToPredefinedDates(predefinedDates, employeeId, cookie);
        }
        System.out.println("Finished");
    }

    private void addToPredefinedDates(List<GetDayViewEvent> predefinedDates, int employeeId, String cookie) throws JsonProcessingException {
        for (GetDayViewEvent it : predefinedDates) {
            String date = LocalDateTime.parse(it.getStart()).toLocalDate().toString();
            addCostToDate(employeeId, cookie, date);
        }
    }

    private void addCostToDate(int employeeId, String cookie, String date) throws JsonProcessingException {
        Form data = createFormData(employeeId, date);
        Client client = null;
        Response post = null;
        try {
            client = createClient(false, false);
            post = postForm(cookie, data, client);
            System.out.println("Cost added successfully to " + date);
        } catch (Exception e) {
            System.err.println("Cost could not be added to " + date + ", " + e.getMessage());
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    private Form createFormData(int employeeId, String date) throws JsonProcessingException {
        Form data = new Form();
        data.param("eventDisableLoader", "true");
        data.param("DayDate", date);
        data.param("StaffId", objectMapper.writeValueAsString(List.of(employeeId)));
        data.param("EmployeeId", employeeId + "");
        data.param("Data", objectMapper.writeValueAsString(List.of(Map.of("CostCode", 25372, "FromTime", "09:00", "ToTime", "18:00", "id", "extModel390-1"))));
        return data;
    }

    private List<GetDayViewEvent> getPredefinedDates(int year, int month, int startDay, int endDay, String cookie) {
        Client client = null;
        Response post = null;
        try {
            String startDate = String.format("%s-%02d-%02d", year, month, startDay);
            String endDate = String.format("%s-%02d-%02d", year, month, endDay);
            client = createClient(false, false);
            post = getDayViewEvents(cookie, startDate, endDate, client);
            if (post.getStatus() == 200) {
                String s = post.readEntity(String.class);
                GetDayViewEventsResult result = new Gson().fromJson(s, GetDayViewEventsResult.class);
                long countOfCDPProject = result.getData().stream().filter(e -> "CDP Project".equals(e.getTitle())).count();
                System.out.println("CDP Project allocated: " + countOfCDPProject);
                return result.getData().stream().filter(e -> "Nincs allokáció".equals(e.getTitle())).collect(Collectors.toList());
            } else {
                System.out.println(post.readEntity(String.class));
            }
        } catch (Exception e) {
            System.err.println("Cannot fetch predefined dates: " + e.getMessage());
        } finally {
            if (client != null) {
                client.close();
            }
        }
        String url = "event=GetDayViewEvents&_dc=1619429105869&startDate=2021-05-03&endDate=2021-05-09&page=1&start=0&limit=25&eventDisableLoader=true";
//        String url = "https://at.codebase.hu/AttendanceTracking/Handlers/AttendanceTracking.Handlers.AttendanceSheetHandler.ashx?event=GetDayViewEvents&_dc=1619429105869&startDate=2021-05-03&endDate=2021-05-09&page=1&start=0&limit=25&eventDisableLoader=true"
        return Collections.emptyList();
    }

    private Response getDayViewEvents(String cookie, String startDate, String endDate, Client client) {
        return client.target("https://at.codebase.hu")
//                    .path("/AttendanceTracking/Handlers/AttendanceTracking.Handlers.AttendanceSheetPlanningHandler.ashx")
                .path("/AttendanceTracking/Handlers/AttendanceTracking.Handlers.AttendanceSheetHandler.ashx")
                .queryParam("event", "GetDayViewEvents")
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .queryParam("page", "1")
                .queryParam("start", "0")
                .queryParam("limit", "31")
                .queryParam("eventDisableLoader", "true")
                .request()
                .header("cookie", cookie)
                .header("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
                .header("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36")
                .header("x-requested-with", "XMLHttpRequest")
                .get();
    }

    private Response postForm(String cookie, Form data, Client client) {
        return client.target("https://at.codebase.hu")
                .path("/AttendanceTracking/Handlers/AttendanceTracking.Handlers.AttendanceSheetPlanningHandler.ashx")
                .queryParam("event", "SaveEfficientWorking")
                .request()
                .header("cookie", cookie)
                .header("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
                .header("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36")
                .header("x-requested-with", "XMLHttpRequest")
                .post(Entity.form(data));
    }

    private LocalDate getLocalDate(String date) {
        try {
            return LocalDate.parse(date);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return null;
    }

    Client createClient(boolean secure, boolean ignorePreValidation) {
        ClientConfig config = new ClientConfig();
        config.property(ClientProperties.FOLLOW_REDIRECTS, "false");
        config.property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT_MS);
//        config.register(MultiPartFeature.class);

        ClientBuilder builder = ClientBuilder.newBuilder().withConfig(config);

        if (!secure) {
            builder.sslContext(CertificateTrustManager.sslContext());
            builder.hostnameVerifier(CertificateTrustManager.hostnameVerifier());
        }

        Client client = builder.build();
        client.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, ignorePreValidation);

        return client;
    }
}
