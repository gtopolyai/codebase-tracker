import java.util.List;

public class GetDayViewEventsResult {

    private boolean success;

    private List<GetDayViewEvent> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<GetDayViewEvent> getData() {
        return data;
    }

    public void setData(List<GetDayViewEvent> data) {
        this.data = data;
    }
}
