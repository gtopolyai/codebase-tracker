import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.glassfish.jersey.SslConfigurator;

public class CertificateTrustManager {

    private CertificateTrustManager() {
    }

    public static HostnameVerifier hostnameVerifier() {

        // Do not verify host names
        return (hostname, sslSession) -> {
            return true;
        };

    }

    public static SSLContext sslContext() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = {trustEverythingTrustManager()};
        try {
            // Install the all-trusting trust manager
            SSLContext sc = SslConfigurator.newInstance().createSSLContext();
            sc.init(null, trustAllCerts, new SecureRandom());
            return sc;
        } catch (KeyManagementException e) {
            throw new RuntimeException("F", e);
        }
    }

    private static X509TrustManager trustEverythingTrustManager() {
        return new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
                // Trust everything
            }

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
                // Trust everything
            }
        };
    }

    public static class SavingX509TrustManager implements X509TrustManager {
        private X509Certificate[] chain;

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) {
            this.chain = chain;
        }

        public X509Certificate[] getChain() {
            return chain;
        }
    }

}