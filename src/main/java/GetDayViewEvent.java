public class GetDayViewEvent {

    private int Clickable;
    private boolean SlotEditable;
    private int WorktimePlanning;
    private boolean ad;
    private int cid;
    private String end;
    private int id;
    private boolean isvirtual;
    private String notes;
    private String start;
    private String title;
    private String url;

    public int getClickable() {
        return Clickable;
    }

    public void setClickable(int clickable) {
        Clickable = clickable;
    }

    public boolean isSlotEditable() {
        return SlotEditable;
    }

    public void setSlotEditable(boolean slotEditable) {
        SlotEditable = slotEditable;
    }

    public int getWorktimePlanning() {
        return WorktimePlanning;
    }

    public void setWorktimePlanning(int worktimePlanning) {
        WorktimePlanning = worktimePlanning;
    }

    public boolean isAd() {
        return ad;
    }

    public void setAd(boolean ad) {
        this.ad = ad;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIsvirtual() {
        return isvirtual;
    }

    public void setIsvirtual(boolean isvirtual) {
        this.isvirtual = isvirtual;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
