import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void testComma() {
        List<Integer> months = CodebaseTrackerApp.getMonths("1,2,3,6");
        Assert.assertArrayEquals(new Integer[]{1, 2, 3, 6}, months.toArray());
    }


    @Test
    public void testRange() {
        List<Integer> months = CodebaseTrackerApp.getMonths("1-6");
        Assert.assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6}, months.toArray());
    }
}
